export default {
  // The blog's title as it appears in the layout header, and in the document
  // <title> tag.
  getImg: " ",
  title: " ",
  author: " ",
  description: " ",

  // The number of posts to a page on the site index.
  indexPageSize: 20
};
