export default {
  getImg: () => import("./work.jpg"),
  title: `¿Qué necesitas para encontrar un empleo? Sin duda, nuestra herramienta te ayudará.`,
  tags: ["Work", "Tips", "Locatefit", "Genial"],
  spoiler:
    "Pasamos largos años de nuestra vida aprendiendo destrezas técnicas que nos aportan datos objetivos a nuestro currículum y que nos sirven para superar el primer filtro de cualquier proceso de selección a la hora de encontrar un empleo.",
  getContent: () => import("./document.mdx")
};
