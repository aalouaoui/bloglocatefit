import React from "react";

import { stack as Menu } from "react-burger-menu";
import styles from "./header2.css";

class Example extends React.Component {
  showSettings(event) {
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <Menu right>
          <a id="contact" className={styles.menuitem1} href="/">
            ¿Quiens Somos?
          </a>
          <a id="home" className={styles.menuitem} href="/">
            Trabajo
          </a>
          <a id="home" className={styles.menuitem} href="/empleo">
            Empleo
          </a>
          <a id="about" className={styles.menuitem} href="/profesionales">
            Profesionales
          </a>

          <a id="home" className={styles.menuitem1} href="/">
            Ayuda
          </a>
          <a id="home" className={styles.menuitem} href="/preguntas-frecuentes">
            Preguntas Frecuentes
          </a>
          <a id="about" className={styles.menuitem} href="/contacto">
            Contacto
          </a>

          <a id="home" className={styles.menuitem1} href="/">
            Términos Legales
          </a>
          <a id="home" className={styles.menuitem} href="/condiciones-de-uso">
            Condiciones de uso
          </a>
          <a id="about" className={styles.menuitem} href="/privacidad">
            Política de privacidad
          </a>
          <a id="about" className={styles.menuitem} href="/cookies">
            Política de Cookies
          </a>
        </Menu>
      </div>
    );
  }
}

export default Example;
