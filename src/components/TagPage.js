import React from "react";
import ArticleSummary from "./ArticleSummary";
import styles from "./TagPage.module.css";

function TagPage({ blogRoot, name, routes }) {
  return (
    <div className={styles.TagPage}>
      <div className={styles.headerindex2}>
        <a className={styles.itemactive} href="/">
          Locatefit
        </a>
        <a className={styles.item} href="/">
          ¿Quienes somos?
        </a>
        <a className={styles.item} href="/">
          Profesionales
        </a>
        <a className={styles.item} href="/">
          Contacto
        </a>
        <a className={styles.item} href="/">
          Empleo
        </a>
        <a className={styles.item1} href="/">
          Publicar un Servicio
        </a>
      </div>

      <div className={styles.headerindex1}>
        <h1 className={styles.headerindextittle}>{name} posts</h1>
      </div>

      <ul className={styles.articlesList}>
        {routes.map(route => (
          <li key={route.url.href}>
            <ArticleSummary blogRoot={blogRoot} route={route} />
          </li>
        ))}
      </ul>
    </div>
  );
}

export default TagPage;
