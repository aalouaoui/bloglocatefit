import React from "react";
import { View, NotFoundBoundary, useLoadingRoute } from "react-navi";
import NotFoundPage from "./NotFoundPage";
import LoadingIndicator from "./LoadingIndicator";
import styles from "./BlogLayout.module.css";
import mainLogo from "./../assets/img/logoblog.png";

function BlogLayout({ isViewingIndex }) {
  let loadingRoute = useLoadingRoute();

  return (
    <div className={styles.container}>
      <div className={styles.navbar}>
        <div className={styles.navbarlogo}>
          <a className={styles.navbarbrand} href="/">
            <img src={mainLogo} width="250px" height="auto" alt="logo" />
          </a>
        </div>
      </div>

      <div className={styles.container1}>
        <LoadingIndicator active={!!loadingRoute} />
        {// Don't show the header on index pages, as it has a special header.
        !isViewingIndex && (
          <header>
            <h3 className={styles.title}> </h3>
          </header>
        )}
        <main>
          <NotFoundBoundary render={() => <NotFoundPage />}>
            <View />
          </NotFoundBoundary>
        </main>
      </div>
    </div>
  );
}

export default BlogLayout;
