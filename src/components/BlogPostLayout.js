import React from "react";
import { Link, useCurrentRoute, useView } from "react-navi";
import { MDXProvider } from "@mdx-js/react";
import siteMetadata from "../siteMetadata";
import ArticleMeta from "./ArticleMeta";
import Bio from "./Bio";
import Sharebutton from "./share";
import styles from "./BlogPostLayout.module.css";

function BlogPostLayout({ blogRoot }) {
  let { title, data, url } = useCurrentRoute();
  let { connect, content, head } = useView();
  let { MDXComponent, readingTime } = content;
  // The content for posts is an MDX component, so we'll need
  // to use <MDXProvider> to ensure that links are rendered
  // with <Link>, and thus use pushState.
  return connect(
    <>
      {head}
      <article className={styles.container}>
        <div className={styles.headercontent}>
          <div className={styles.header}>
            <header>
              <h1 className={styles.title}>
                <Link href={url.pathname}>{title}</Link>
              </h1>

              <Bio className={styles.bio} />

              <div className={styles.bio}>
                <ArticleMeta
                  blogRoot={blogRoot}
                  meta={data}
                  readingTime={readingTime}
                />

                <Sharebutton className={styles.Sharebutton} />
              </div>
            </header>
          </div>
        </div>

        <MDXProvider
          components={{
            a: Link,
            wrapper: ({ children }) => (
              <div className={styles.content}>{children}</div>
            )
          }}
        >
          <MDXComponent />
        </MDXProvider>
        <footer className={styles.footer}>
          <h3 className={styles.title}>
            <Link href={blogRoot}>{siteMetadata.title}</Link>
          </h3>

          <div className={styles.related}>
            <h2 className={styles.relatedtittle}>Continua leyendo</h2>

            <div className={styles.previousDetails}>
              {data.previousDetails && (
                <Link
                  className={styles.previous}
                  href={data.previousDetails.href}
                >
                  ← {data.previousDetails.title}
                </Link>
              )}
            </div>

            <div className={styles.nextDetails}>
              {data.nextDetails && (
                <Link className={styles.next} href={data.nextDetails.href}>
                  {data.nextDetails.title} →
                </Link>
              )}
            </div>

            <div
              className="fb-comments"
              data-href="https://developers.facebook.com/docs/plugins/comments#configurator"
              data-width=""
              data-numposts="10"
            ></div>
          </div>
        </footer>
      </article>
    </>
  );
}

export default BlogPostLayout;
