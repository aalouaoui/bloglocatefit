import React from "react";
import { useCurrentRoute } from "react-navi";
import styles from "./share.module.css";

import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton
} from "react-share";

import {
  FacebookIcon,
  TwitterIcon,
  WhatsappIcon,
  LinkedinIcon
} from "react-share";

function Sharebutton() {
  let { url } = useCurrentRoute();

  return (
    <div className={styles.Sharebutton}>
      <FacebookShareButton className={styles.Sharebutton1} url={url.pathname}>
        <FacebookIcon size={40} round={true} />
      </FacebookShareButton>

      <TwitterShareButton className={styles.Sharebutton2} url={url.pathname}>
        <TwitterIcon size={40} round={true} />
      </TwitterShareButton>

      <LinkedinShareButton className={styles.Sharebutton3} url={url.pathname}>
        <LinkedinIcon size={40} round={true} />
      </LinkedinShareButton>

      <WhatsappShareButton className={styles.Sharebutton4} url={url.pathname}>
        <WhatsappIcon size={40} round={true} />
      </WhatsappShareButton>
    </div>
  );
}

export default Sharebutton;
