import React from "react";
import styles from "./NotFoundPage.module.css";
import Depresion from "./../assets/img/depresion.svg";
import { FaArrowLeft, FaPlus } from "react-icons/fa";

// Note that create-react-navi-app will always show an error screen when this
// is rendered. This is because the underlying react-scripts package shows
// the error screen when a NotFoundError is thrown, even though it's caught
// by <NotFoundBoundary>. To see the error rendered by this function,
// you'll just need to close the error overlay with the "x" at the top right.
function NotFoundPage() {
  return (
    <div className={styles.NotFound}>
      <div className={styles.headerindex2}>
        <a className={styles.itemactive} href="/">
          Locatefit
        </a>
        <a className={styles.item} href="/">
          ¿Quienes somos?
        </a>
        <a className={styles.item} href="/">
          Profesionales
        </a>
        <a className={styles.item} href="/">
          Contacto
        </a>
        <a className={styles.item} href="/">
          Empleo
        </a>
        <a className={styles.item1} href="/">
          {" "}
          <FaPlus /> Publicar un Servicio
        </a>
      </div>

      <div className={styles.headerindex1}>
        <h1 className={styles.headerindextittle}>404 Pagina no encontrada</h1>
      </div>
      <div className={styles.depresion}>
        <img className={styles.depresionimg} src={Depresion} alt="depression" />
        <h3 className={styles.depresiontittle}>
          Parece que no podemos encontrar lo que estás buscando.
        </h3>

        <a className={styles.item1} href="/">
          <FaArrowLeft /> Volver al inicio
        </a>
      </div>
    </div>
  );
}

export default NotFoundPage;
