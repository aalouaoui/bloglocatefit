import React from "react";
import ArticleSummary from "./ArticleSummary";
import Pagination from "./Pagination";
import styles from "./BlogIndexPage.module.css";
import imagen1 from "./../assets/img/imagen1.jpg";

function BlogIndexPage({ blogRoot, pageCount, pageNumber, postRoutes }) {
  return (
    <div className={styles.containerindex}>
      <div className={styles.headerindex1}>
        <div className={styles.titulodelblog}>
          <h1 className={styles.headerindextittle}>
            Bienvedidos a nuestro blog enterate de todas las noticias de nuestra
            comunidad
          </h1>
          <p>
            Somos el equipo de contenido, investigación y diseño de Deliveroo.
            Ayudamos a los pasajeros a moverse, los restaurantes crecen y los
            clientes comen en más de 200 ciudades de todo el mundo. Ven a buscar
            las respuestas con nosotros.
          </p>
          <a href="/">Conviertete en profesional </a>
        </div>
      </div>

      <div className={styles.Wrapper}>
        <div className={styles.Container}>
          <div className={styles.Contenidoimagen}>
            <img src={imagen1} width="560px" height="392px" alt="image1" />
          </div>

          <div className={styles.Contenidotexto}>
            <h3>Apoyando a 30,000 jinetes</h3>
            <p>
              En el equipo de Delivery, creamos productos que tienen un gran
              impacto en los pasajeros en 12 países. Nos reunimos con pasajeros,
              escuchamos sus historias y trabajamos junto a sistemas complejos
              para construir las herramientas que permiten que nuestra red de
              entrega funcione.
            </p>
          </div>
        </div>

        <div className={styles.Container}>
          <div className={styles.Contenidoimagen1}>
            <img src={imagen1} width="560px" height="392px" alt="image1" />
          </div>

          <div className={styles.Contenidotexto1}>
            <h3>Apoyando a 30,000 jinetes</h3>
            <p>
              En el equipo de Delivery, creamos productos que tienen un gran
              impacto en los pasajeros en 12 países. Nos reunimos con pasajeros,
              escuchamos sus historias y trabajamos junto a sistemas complejos
              para construir las herramientas que permiten que nuestra red de
              entrega funcione.
            </p>
          </div>
        </div>

        <div className={styles.Container}>
          <div className={styles.Contenidoimagen2}>
            <img src={imagen1} width="560px" height="392px" alt="image1" />
          </div>

          <div className={styles.Contenidotexto2}>
            <h3>Apoyando a 30,000 jinetes</h3>
            <p>
              En el equipo de Delivery, creamos productos que tienen un gran
              impacto en los pasajeros en 12 países. Nos reunimos con pasajeros,
              escuchamos sus historias y trabajamos junto a sistemas complejos
              para construir las herramientas que permiten que nuestra red de
              entrega funcione.
            </p>
          </div>
        </div>
      </div>

      <div className={styles.postdelblog}>
        <div>
          <h1>Lee nuestro blog</h1>
          <p>
            Cosas en las que estamos trabajando, eventos a los que vamos,
            productos que usamos y personas que conocemos.
          </p>
        </div>

        <ul className={styles.articlesList}>
          {postRoutes.map(route => (
            <li key={route.url.href}>
              <ArticleSummary blogRoot={blogRoot} route={route} />
            </li>
          ))}
        </ul>
        {pageCount > 1 && (
          <Pagination
            blogRoot={blogRoot}
            pageCount={pageCount}
            pageNumber={pageNumber}
          />
        )}
      </div>
    </div>
  );
}

export default BlogIndexPage;
