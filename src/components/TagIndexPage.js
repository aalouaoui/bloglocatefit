import React from "react";
import { Link } from "react-navi";
import styles from "./TagIndexPage.module.css";

function TagIndexPage(props) {
  return (
    <div className={styles.TagIndexPage}>
      <div className={styles.headerindex2}>
        <a className={styles.itemactive} href="/">
          Locatefit
        </a>
        <a className={styles.item} href="/">
          ¿Quienes somos?
        </a>
        <a className={styles.item} href="/">
          Profesionales
        </a>
        <a className={styles.item} href="/">
          Contacto
        </a>
        <a className={styles.item} href="/">
          Empleo
        </a>
        <a className={styles.item1} href="/">
          Publicar un Servicio
        </a>
      </div>

      <div className={styles.headerindex1}>
        <h1 className={styles.headerindextittle}>Etiquetas</h1>
      </div>

      <ul className={styles.tags}>
        {props.tags.map(tag => (
          <li key={tag.href}>
            <Link href={tag.href}>
              {tag.name} ({tag.count})
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default TagIndexPage;
